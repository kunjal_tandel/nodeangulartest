import express from 'express';
import bodyParser from 'body-parser';
import {join} from 'path';
import http from 'http';
import multer from 'multer';
import {path as ffmpegPath} from '@ffmpeg-installer/ffmpeg';
import {path as ffprobePath} from '@ffprobe-installer/ffprobe';
import ffmpeg from 'fluent-ffmpeg';
import sharp from 'sharp';
import fs from 'fs';
import videoshow from 'videoshow';
import socketIo from 'socket.io';

const app = express();
ffmpeg.setFfmpegPath(ffmpegPath);

ffmpeg.setFfprobePath(ffprobePath);
videoshow.ffmpeg = ffmpeg;
let timemark = null;

const server = http.createServer();
const io = socketIo(server);
io.on('connection', function (client) {
  console.log('connected');
});
server.listen(3000,'0.0.0.0');

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, join(__dirname, 'img/'))
  },
  filename: (req, file, cb) => {
    var file1 = file.originalname.split(".");
    var date = Date.now();
    req.body.file = 'img' + "_" + date + "." + file1[file1.length - 1];
    cb(null, req.body.file);
  }
});
var upload = multer({ storage: storage }).fields([{name: 'picture', maxCount: 1},{name: 'gallary', maxCount: 50 }]);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

app.use(express.static(join(__dirname, 'dist')));
app.use(express.static(join(__dirname, 'img')));
app.all("/*", (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  next();
});

app.post('/upload', upload, async (req, res) => {
  const addWatermark = (filepath) => {
    return new Promise((resolve, reject) => {
      sharp(filepath)
        .overlayWith(join(__dirname, 'img/watermark.png'), {top: 50, left: 50, density: 100})
        .toBuffer((err, outputBuffer) => {
          if(!err) {
            fs.writeFile(filepath, outputBuffer,  "binary",function(err) {
              resolve();
            });
          }
        });
    });
  };

  const resize = (filepath, name) => {
    return new Promise(async (resolve, reject) => {
      const waterMarks = await addWatermark(filepath);

      sharp(filepath)
        .resize(200, 200)
        .toFile(join(__dirname, 'img/thumbnail/' + name), async (err) => {
          if(err) {
            return reject(err);
          }
          const waterMarks = await addWatermark(join(__dirname, 'img/thumbnail/' + name));
          resolve();
        });
    })
  };

  let _promise = [];
  req.files.gallary.forEach((file) => {
    _promise.push(resize(file.path, file.filename));
  });

  try {
    const waterMarks  = await Promise.all(_promise);
    res.send(req.files);
  } catch (err) {
    res.send(req.files);
  }
});

app.post('/createVideo', (req, res) => {

  let _promise = [];
  req.body.gallary.forEach((file, i) => {
    _promise.push(file.path);
  });
  let totalFiles = req.body.gallary.length;
  var videoOptions = {
    fps: 25,
    loop: 2, // seconds
    transition: true,
    transitionDuration: 1,
    videoBitrate: 1024,
    videoCodec: 'libx264',
    size: '640x?',
    format: 'mp4',
    pixelFormat: 'yuv420p'
  };
  let percent = [];
  let video =  'video'+Date.now()+'.mp4';
  videoshow(_promise, videoOptions)
    .logo(join(__dirname, 'img/watermark.png'), {start: 0, end: _promise.length * 2, xAxis: 50, yAxis: 50})
    .save(join(__dirname, 'img/'+video))
    .on('progress', (data) => {
      if(data.percent === 0 && percent.length) {
        totalFiles -=1;
      }
      percent.push(data.percent);
      io.emit(req.query.progressId, JSON.stringify({percent: data.percent, files: totalFiles}));
    })
    .on('error', (err, stdout, stderr) => {
      console.error('Error:', err);
      console.error('ffmpeg stderr:', stderr);
      res.status(500).send(err);
    })
    .on('end', (output) => {
      res.send({video: video});
    })
});

app.listen(4000, '0.0.0.0', () => {
  console.log('Server running on 8080')
});

import { Component, OnInit } from '@angular/core';
import {HttpEventType, HttpClient, HttpRequest} from "@angular/common/http";
import {Http} from "@angular/http";
import * as IO from 'socket.io-client';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  inputFiles: File[];
  uploadingProgressing = false;
  uploadProgress = 0;
  videoProgress = 0;
  waitForProgress = false;
  uploadDone = false;
  videoDone = false;
  uploadResponse;
  videoResponse;
  countVideo = 0;
  socket;
  videoProcessing = false;
  constructor(private http: HttpClient, private http2: Http) {
  }

  ngOnInit() {
  }

  handleProgress(event) {
    if (event.type === HttpEventType.UploadProgress) {
      this.uploadingProgressing =true;
      this.uploadProgress = Math.round(100 * event.loaded / event.total) - 0.85;
      console.log('uploaded: ', this.uploadProgress);
    }

    if (event.type === HttpEventType.Response) {
      this.uploadingProgressing = false;
      this.uploadResponse = event.body;
      this.uploadDone = true;
      console.log('upload response: ', this.uploadResponse);
    }
  }

  handelInputFiles(files: File[]) {
    this.uploadResponse = null;
    this.videoDone = false;
    this.uploadDone = false;
    this.videoResponse = null;
    this.inputFiles = [];
    this.inputFiles = files;
    let formDate = new FormData();
    for(let i=0; i < this.inputFiles.length; i++) {
      formDate.append('gallary', this.inputFiles[i]);
    }

    const req = new HttpRequest('POST', 'http://52.23.188.220:4000/upload', formDate, {
      reportProgress: true
    });
    this.http.request(req)
      .subscribe(
        event=>this.handleProgress(event),
        error=>{
          console.log("Server error")
        });
  }

  randomString() {
    let str = '';
    let all = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for(let i = 0; i < 15; i++ ) {
      str += all[parseInt((Math.random() * all.length).toFixed(2))];
    }

    return str;
  }

  generateVideo(){
    if(!this.uploadResponse) return;
    this.waitForProgress = true;
    let str = this.randomString()+Date.now();
    let sock = IO.connect('http://52.23.188.220:3000');
    sock.on(str, (data) => {
      this.videoProcessing = true;
      this.waitForProgress = false;
      let j = JSON.parse(data);
      this.videoProgress = parseFloat((j.percent < 100 ? j.percent : this.videoProgress).toFixed(2))
      this.countVideo = this.uploadResponse.gallary.length - j.files;
    });
    this.http2.post('http://52.23.188.220:4000/createVideo?progressId='+str, this.uploadResponse)
      .subscribe(value => {
        this.videoResponse = JSON.parse(value['_body'])['video']; this.videoProcessing=false;this.videoDone=true;
        window.open('http://52.23.188.220:4000/'+this.videoResponse, '_target');
        sock.disconnect();
      }, error2 => {
        this.videoProcessing=false;this.videoDone=true;
        sock.disconnect();
      });
  }

}
